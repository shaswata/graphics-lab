package components;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Point;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class DDALine {

	public static void main(String[] args) {
		BufferedImage buffered_image = new BufferedImage(400, 400, BufferedImage.TYPE_INT_RGB);
		
		ImageIcon image = new ImageIcon(buffered_image);
		
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(buffered_image.getWidth(), buffered_image.getHeight());
		frame.setLayout(new FlowLayout());
		frame.add(new JLabel(image));
		
		frame.setVisible(true);
		
		drawDDALine(buffered_image, frame, new Point(10, 10), new Point(200, 300), Color.ORANGE, 2);
		drawDDALine(buffered_image, frame, new Point(10, 300), new Point(200, 10), Color.PINK, 2);
		drawDDALine(buffered_image, frame, new Point(10, 10), new Point(10, 300), Color.CYAN, 2);
		drawDDALine(buffered_image, frame, new Point(10, 300), new Point(200, 300), Color.YELLOW, 2);
		drawDDALine(buffered_image, frame, new Point(10, 10), new Point(200, 10), Color.WHITE, 2);
		drawDDALine(buffered_image, frame, new Point(200, 10), new Point(200, 300), Color.LIGHT_GRAY, 2);
		
		Color arr[] = new Color[4];
		arr[0] = Color.RED;
		arr[1] = Color.GREEN;
		arr[2] = Color.pink;
		arr[3] = Color.YELLOW;
	
		for(int i = 0; i < 18; i++ ) {
			drawDDALine(buffered_image, frame, new Point(200, 300), new Point(20 + (i*10), 10), arr[i%4], 1);
			drawDDALine(buffered_image, frame, new Point(190 - (i*10), 10), new Point(10, 300), arr[i%4], 1);
		}
			
		System.out.println("!!!!!!!!DONE!!!!!!!!");
	}
	
	public static void drawDDALine(BufferedImage image, JFrame frame, Point p1, Point p2, Color color, int sleep) {
		int steps = 0;
		
		if(p1.x > p2.x) {
			Point temp = p1;
			p1 = p2;
			p2 = temp;
		}
		
		int del_x = Math.abs(p2.x - p1.x);
		int del_y = Math.abs(p2.y - p1.y); 
		
		if(del_x >= del_y)
			steps = del_x;
		else
			steps = del_y;
		
		System.out.println("STEPS = " + steps);
		
		double x_inc = (double)del_x / (double)steps;
		double y_inc = (double)del_y / (double)steps;
		
		if(p1.y > p2.y) 
			y_inc = -y_inc;
		
		System.out.println("DEL X = " + del_x + ", X_INC = " + x_inc);
		System.out.println("DEL Y = " + del_y + ", Y_INC = " + y_inc);
		
		double x = p1.x;
		double y = p1.y;
		int step_count = 0;
		
		while(step_count <= steps) {
			int x_plt = (int) Math.round(x);
			int y_plt = (int) Math.round(y);
			System.out.printf("(%d, %d)\n", x_plt, y_plt);
			image.setRGB(x_plt, y_plt, color.getRGB());
			frame.repaint();
			x += x_inc;
			y += y_inc;
			step_count++;
			try {
				Thread.sleep(sleep);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
