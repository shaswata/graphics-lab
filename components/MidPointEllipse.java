package components;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Point;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class MidPointEllipse {

	public static void main(String[] args) {
		BufferedImage buffered_image = new BufferedImage(400, 400, BufferedImage.TYPE_INT_RGB);
		
		ImageIcon image = new ImageIcon(buffered_image);
		
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(buffered_image.getWidth(), buffered_image.getHeight());
		frame.setLayout(new FlowLayout());
		frame.add(new JLabel(image));
		
		frame.setVisible(true);
		
		drawMidPointEllipse(buffered_image, frame, new Point(200, 200), 100, 70, Color.PINK, 2);
		drawMidPointEllipse(buffered_image, frame, new Point(200, 200), 70, 100, Color.GREEN, 2);
		drawMidPointEllipse(buffered_image, frame, new Point(200, 200), 100, 100, Color.YELLOW, 2);
			
		System.out.println("!!!!!!!!DONE!!!!!!!!");
	}
	
	public static void drawMidPointEllipse(BufferedImage image, JFrame frame, Point center, int rX, int rY, Color color, int sleep) {
		int x = 0;
		int y = rY;
		
		int rXsq = rX * rX;
		int rYsq = rY * rY;
		
		plotEllipseMirror(frame, image, center, new Point(x, y), color, sleep);
		
		// Plot Region 1
		double p = rYsq - (rXsq * rY) + (0.25 * rXsq);
		int pX = 0; 
		int pY = 2 * rXsq * y;
		while(pX < pY) {
			x++; // increment x
			pX += 2 * rYsq;
			if(p < 0) {
				p += rYsq + pX;
			} else {
				y--;
				pY -= 2 * rXsq;
				p += rYsq + (pX - pY); 
			}
			plotEllipseMirror(frame, image, center, new Point(x, y), color, sleep);
		}
		
		// Plot Region 2
		p = (rYsq * ((x + 0.5)*(x + 0.5))) + (rXsq * (y - 1)*(y - 1)) - (rXsq * rYsq);
		System.out.println("P = " + p);
		while(y > 0) {
			y--;
			pY -= 2 * rXsq;
			if(p > 0) {
				p += rXsq - pY;
			} else {
				x++;
				pX += 2 * rYsq;
				p += rXsq - pY + pX;
			}
			plotEllipseMirror(frame, image, center, new Point(x, y), color, sleep);
		}
		
	}
	
	private static void plotEllipseMirror(JFrame frame, BufferedImage image, Point center, Point plot, Color color, int sleep) {
		image.setRGB(center.x + plot.x, center.y + plot.y, color.getRGB());
		image.setRGB(center.x + plot.x, center.y - plot.y, color.getRGB());
		image.setRGB(center.x - plot.x, center.y + plot.y, color.getRGB());
		image.setRGB(center.x - plot.x, center.y - plot.y, color.getRGB());
		System.out.printf("(%d, %d)", center.x + plot.x, center.y + plot.y);
		System.out.printf("(%d, %d)", center.x + plot.x, center.y - plot.y);
		System.out.printf("(%d, %d)", center.x - plot.x, center.y + plot.y);
		System.out.printf("(%d, %d)", center.x - plot.x, center.y - plot.y);
		System.out.println();
		try {
			if(frame != null)
				frame.repaint();
			Thread.sleep(sleep);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
