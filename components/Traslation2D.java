package components;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class Traslation2D {
	public static void main(String args[]) {
		BufferedImage buffered_image = new BufferedImage(400, 400, BufferedImage.TYPE_INT_RGB);
		ImageIcon image = new ImageIcon(buffered_image);
		
		JFrame frame = new JFrame();
		frame.setSize(400, 400);
		frame.setLayout(new FlowLayout());
		frame.add(new JLabel(image));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		
		Graphics g = buffered_image.getGraphics();
		
		Point p1 = new Point(50, 50);
		Point p2 = new Point(100, 100);
		Point p3 = new Point(50, 100);
		
		g.setColor(Color.CYAN);
		g.drawLine(p1.x, p1.y, p2.x, p2.y);
		g.drawLine(p1.x, p1.y, p3.x, p3.y);
		g.drawLine(p3.x, p3.y, p2.x, p2.y);
		g.drawString("Original", 10, 10);
		frame.repaint();
		JOptionPane.showMessageDialog(frame, "Click to Proceed");
		
		g.setColor(Color.GREEN);
		translatePoint(p1, 30, 40);
		translatePoint(p2, 30, 40);
		translatePoint(p3, 30, 40);
		g.drawLine(p1.x, p1.y, p2.x, p2.y);
		g.drawLine(p1.x, p1.y, p3.x, p3.y);
		g.drawLine(p3.x, p3.y, p2.x, p2.y);
		g.drawString("Translated", 10, 390);
		frame.repaint();
		JOptionPane.showMessageDialog(frame, "Click to Proceed");
		
		g.setColor(Color.PINK);
		rotatePoint(p1, new Point(110, 110), 10);
		rotatePoint(p2, new Point(110, 110), 10);
		rotatePoint(p3, new Point(110, 110), 10);
		g.drawLine(p1.x, p1.y, p2.x, p2.y);
		g.drawLine(p1.x, p1.y, p3.x, p3.y);
		g.drawLine(p3.x, p3.y, p2.x, p2.y);
		g.drawString("Rotated", 10, 390);
		frame.repaint();
		JOptionPane.showMessageDialog(frame, "Click to Proceed");
		
		g.setColor(Color.RED);
		scalePoint(p1, new Point(110, 110), 1.8, 1.8);
		scalePoint(p2, new Point(110, 110), 1.8, 1.8);
		scalePoint(p3, new Point(110, 110), 1.8, 1.8);
		g.drawLine(p1.x, p1.y, p2.x, p2.y);
		g.drawLine(p1.x, p1.y, p3.x, p3.y);
		g.drawLine(p3.x, p3.y, p2.x, p2.y);
		g.drawString("Scaled", 10, 370);
		frame.repaint();
		JOptionPane.showMessageDialog(frame, "Click to Proceed");
		
		g.setColor(Color.RED);
		shearPoint(p1, 2);
		shearPoint(p2, 2);
		shearPoint(p3, 2);
		g.drawLine(p1.x, p1.y, p2.x, p2.y);
		g.drawLine(p1.x, p1.y, p3.x, p3.y);
		g.drawLine(p3.x, p3.y, p2.x, p2.y);
		g.drawString("Scaled", 10, 370);
		frame.repaint();
		JOptionPane.showMessageDialog(frame, "Click to Proceed");
	}
	
	public static void translatePoint(Point p, int tx, int ty) {
		p.x += tx;
		p.y += ty;
	}
	
	public static void rotatePoint(Point p, Point center, int degree) {
		double radian = (degree * Math.PI)/180;
		p.x -= center.x;
		p.y -= center.y;
		p.x = (int) (p.x * Math.cos(radian) - p.y * Math.sin(radian));
		p.y = (int) (p.x * Math.sin(radian) + p.y * Math.cos(radian));
		p.x += center.x;
		p.y += center.y;
	}
	
	public static void scalePoint(Point p, Point center, double sx, double sy) {
		p.x -= center.x;
		p.y -= center.y;
		p.x *= sx;
		p.y *= sy;
		p.x += center.x;
		p.y += center.y;
	}
	
	public static void shearPoint(Point p, int sh) {
		p.x += sh * p.y;
	}
}