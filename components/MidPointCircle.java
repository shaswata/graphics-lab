package components;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Point;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class MidPointCircle {

	public static void main(String[] args) {
		BufferedImage buffered_image = new BufferedImage(400, 400, BufferedImage.TYPE_INT_RGB);
		
		ImageIcon image = new ImageIcon(buffered_image);
		
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(buffered_image.getWidth(), buffered_image.getHeight());
		frame.setLayout(new FlowLayout());
		frame.add(new JLabel(image));
		
		frame.setVisible(true);
		
		drawMidPointCircle(buffered_image, frame, new Point(200, 200), 100, Color.GREEN, 10);
		drawMidPointCircle(buffered_image, frame, new Point(200, 200), 80, Color.PINK, 10);
		drawMidPointCircle(buffered_image, frame, new Point(200, 200), 60, Color.BLUE, 10);
		drawMidPointCircle(buffered_image, frame, new Point(200, 200), 40, Color.YELLOW, 10);

		System.out.println("!!!!!!!!DONE!!!!!!!!");
	}
	
	public static void drawMidPointCircle(BufferedImage image, JFrame frame, Point center, int radius, Color color, int sleep) {
		int p = 1 - radius;
		
		int x = radius;
		int y = 0;
		
		System.out.printf("(%d, %d)", x, y);	
		
		// CENTER
		image.setRGB(center.x, center.y, Color.PINK.getRGB());
		
		// Plot 1st Point
		image.setRGB(x + center.x, y + center.y, color.getRGB());
		image.setRGB(-x + center.x, y + center.y, color.getRGB());
		image.setRGB(y + center.y, x + center.x, color.getRGB());
		image.setRGB(y + center.y, -x + center.x, color.getRGB());

		if(frame != null)
			frame.repaint();
		
		while(x > y) {
			y++;
			if(p <= 0) {
				p += (2*y) + 1;
			} else {
				x--;
				p += (2*y) - (2*x) +1;
			}
			
			plotCircleMirroredPixels(image, center, x, y, color);
			
			if(frame != null)
				frame.repaint();
			
			try {
				Thread.sleep(sleep);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}	
		}
	}
	
	private static void plotCircleMirroredPixels(BufferedImage image, Point center, int x, int y, Color color) {
		image.setRGB(x + center.x, y + center.y, color.getRGB());
		image.setRGB(y + center.y, x + center.x, color.getRGB());
		image.setRGB(-x + center.x, y + center.y, color.getRGB());
		image.setRGB(y + center.y, -x + center.x, color.getRGB());
		image.setRGB(-y + center.y, -x + center.x, color.getRGB());
		image.setRGB(-x + center.x, -y + center.y, color.getRGB());
		image.setRGB(-y + center.y, x + center.x, color.getRGB());
		image.setRGB(x + center.x, -y + center.y, color.getRGB());
	}
}
