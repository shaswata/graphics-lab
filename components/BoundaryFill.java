package components;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class BoundaryFill {
	
	public static void main(String args[]) {
		BufferedImage buffered_image = new BufferedImage(400, 400, BufferedImage.TYPE_INT_RGB);
		ImageIcon image = new ImageIcon(buffered_image);
		
		JFrame frame = new JFrame();
		frame.setSize(400, 400);
		frame.setLayout(new FlowLayout());
		frame.add(new JLabel(image));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		
		Graphics g = buffered_image.getGraphics();
		
		g.setColor(Color.CYAN);
		g.drawLine(50, 50, 100, 100);
		g.drawLine(100, 100, 50, 100);
		g.drawLine(50, 100, 50, 50);
		frame.repaint();
		
		
		boundaryFill(55, 90, Color.RED, Color.CYAN, buffered_image, frame);
		frame.repaint();
	}
	
	public static void boundaryFill(int x, int y, Color foreground, Color background, BufferedImage image, JFrame frame) {
		if(image.getRGB(x, y) != background.getRGB() && image.getRGB(x, y) != foreground.getRGB()) {
			image.setRGB(x, y, foreground.getRGB());
			boundaryFill(x+1, y, foreground, background, image, frame);
			boundaryFill(x, y+1, foreground, background, image, frame);
			boundaryFill(x-1, y, foreground, background, image, frame);
			boundaryFill(x, y-1, foreground, background, image, frame);
		}
		try {
			Thread.sleep(10);
			frame.repaint();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
