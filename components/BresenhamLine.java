package components;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Point;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class BresenhamLine {

	public static void main(String[] args) {
		BufferedImage buffered_image = new BufferedImage(400, 400, BufferedImage.TYPE_INT_RGB);
		
		ImageIcon image = new ImageIcon(buffered_image);
		
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(buffered_image.getWidth(), buffered_image.getHeight());
		frame.setLayout(new FlowLayout());
		frame.add(new JLabel(image));
		
		frame.setVisible(true);
		
		drawBresLine(buffered_image, frame, new Point(10, 10), new Point(200, 300), Color.ORANGE, 2);
		drawBresLine(buffered_image, frame, new Point(10, 300), new Point(200, 10), Color.PINK, 2);
		drawBresLine(buffered_image, frame, new Point(10, 10), new Point(10, 300), Color.CYAN, 2);
		drawBresLine(buffered_image, frame, new Point(10, 300), new Point(200, 300), Color.YELLOW, 2);
		drawBresLine(buffered_image, frame, new Point(10, 10), new Point(200, 10), Color.WHITE, 2);
		drawBresLine(buffered_image, frame, new Point(200, 10), new Point(200, 300), Color.LIGHT_GRAY, 2);
		
		Color arr[] = new Color[4];
		arr[0] = Color.RED;
		arr[1] = Color.GREEN;
		arr[2] = Color.PINK;
		arr[3] = Color.YELLOW;
	
		for(int i = 0; i < 18; i++ ) {
			drawBresLine(buffered_image, frame, new Point(200, 300), new Point(20 + (i*10), 10), arr[i%4], 1);
			drawBresLine(buffered_image, frame, new Point(190 - (i*10), 10), new Point(10, 300), arr[i%4], 1);
		}
			
		System.out.println("!!!!!!!!DONE!!!!!!!!");
	}
	
	public static void drawBresLine(BufferedImage image, JFrame frame, Point p1, Point p2, Color color, int sleep) {
		int del_x = Math.abs(p2.x - p1.x);
		int del_y = Math.abs(p2.y - p1.y);
		int sign_x = sign(p2.x - p1.x);
		int sign_y = sign(p2.y - p1.y);
		
		System.out.printf("DEL X,Y : (%d, %d)\n",del_x, del_y);
		System.out.printf("SIGN : (%d, %d)\n", sign_x, sign_y);
		
		int swap = 0;
		if(del_y > del_x) {
			del_x = del_x + del_y;
			del_y = del_x - del_y;
			del_x = del_x - del_y;
			swap = 1;
			System.out.println("SWAP : " + swap);
			System.out.printf("DEL X,Y : (%d, %d)\n",del_x, del_y);
		}
		
		System.out.println("SWAP : " + swap);
		
		int steps = 1;
		
		int p = (2 * del_y) - del_x;
		
		int x = p1.x;
		int y = p1.y;
		
		image.setRGB(x, y, color.getRGB());
		System.out.printf("(%d, %d)\n", x, y);
		
		while(steps <= del_x) {
			if(p >= 0) {
				x = Math.abs(x + sign_x);
				y = Math.abs(y + sign_y);
				p = p + (2 * (del_y - del_x));
			} else {
				if(swap == 1)
					y = Math.abs(y + sign_y);
				else
					x = Math.abs(x + sign_x);
				System.out.printf("SIGN : (%d, %d)\n", sign_x, sign_y);
				p = p + (2 * del_y);
			}
			
			image.setRGB(x, y, color.getRGB());
			System.out.printf("(%d, %d)\n", x, y);
			
			steps++;
			
			if(frame != null)
				frame.repaint();
			
			try {
				Thread.sleep(sleep);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	private static int sign(int x) {
		if(x == 0)
			return 0;
		return x/Math.abs(x);
	}
}
