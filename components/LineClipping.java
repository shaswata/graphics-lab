package components;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class LineClipping {
	
	private static int INSIDE = 0, LEFT = 1, RIGHT = 2, TOP = 4, BOTTOM = 8;
	private static int X_MIN = 100, Y_MIN = 100, X_MAX = 300, Y_MAX = 300;
	
	public static void main(String args[]) {
		BufferedImage buffered_image = new BufferedImage(400,400, BufferedImage.TYPE_INT_RGB);
		ImageIcon image = new ImageIcon(buffered_image);
		
		JFrame frame = new JFrame();
		frame.setSize(400, 400);
		frame.setLayout(new FlowLayout());
		frame.add(new JLabel(image));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		
		clipCohenSuderland(new Point(10, 50), new Point(370, 320), buffered_image);
		clipCohenSuderland(new Point(370, 50), new Point(10, 320), buffered_image);
		clipCohenSuderland(new Point(10, 250), new Point(210, 320), buffered_image);
		clipCohenSuderland(new Point(210, 250), new Point(10, 320), buffered_image);
		clipCohenSuderland(new Point(10, 350), new Point(370, 320), buffered_image);
		frame.repaint();
	}
	
	public static void clipCohenSuderland(Point p1, Point p2, BufferedImage buffered_image) {
		Graphics g = buffered_image.getGraphics();
		g.setColor(Color.CYAN);
		g.drawRect(X_MIN, Y_MIN, (X_MAX-X_MIN), (Y_MAX-Y_MIN));
		g.setColor(Color.RED);
		g.drawLine(p1.x, p1.y, p2.x, p2.y);
		g.setColor(Color.GREEN);
		
		int code1 = computeRegionCode(p1.x, p1.y);
		int code2 = computeRegionCode(p2.x, p2.y);
		
		boolean accept = false;
		
		while(true) {
			if((code1 == 0) && (code2 == 0)) {
	            // If both endpoints lie within rectangle
	            accept = true;
	            break;
	        } else if((code1 & code2) != 0) {
	            // If both endpoints are outside rectangle,
	            // in same region
	            break;
	        } else {
	            // Some segment of line lies within the
	            // rectangle
	            int code_out;
	            double x = 0, y = 0;
	 
	            // At least one endpoint is outside the 
	            // rectangle, pick it.
	            if (code1 != 0)
	                code_out = code1;
	            else
	                code_out = code2;
	 
	            // Find intersection point;
	            // using formulas y = y1 + slope * (x - x1),
	            // x = x1 + (1 / slope) * (y - y1)
	            if ((code_out & TOP) != 0) {
	                // point is above the clip rectangle
	                x = p1.x + (p2.x - p1.x) * (Y_MIN - p1.y) / (p2.y - p1.y);
	                y = Y_MIN;
	            } else if ((code_out & BOTTOM) != 0) {
	                // point is below the rectangle
	                x = p1.x + (p2.x - p1.x) * (Y_MAX - p1.y) / (p2.y - p1.y);
	                y = Y_MAX;
	            } else if ((code_out & RIGHT) != 0) {
	                // point is to the right of rectangle
	                y = p1.y + (p2.y - p1.y) * (X_MAX - p1.x) / (p2.x - p1.x);
	                x = X_MAX;
	            } else if ((code_out & LEFT) != 0) {
	                // point is to the left of rectangle
	                y = p1.y + (p2.y - p1.y) * (X_MIN - p1.x) / (p2.x - p1.x);
	                x = X_MIN;
	            }
	 
	            // Now intersection point x,y is found
	            // We replace point outside rectangle
	            // by intersection point
	            if (code_out == code1) {
	                p1.x = (int)x;
	                p1.y = (int)y;
	                code1 = computeRegionCode(p1.x, p1.y);
	            } else {
	                p2.x = (int)x;
	                p2.y = (int)y;
	                code2 = computeRegionCode(p2.x, p2.y);
	            }
	        }
		}
		
		if(accept) {
			System.out.printf("Line Accepted From : (%d, %d) to (%d, %d).\n", p1.x, p1.y, p2.x, p2.y);
			g.drawLine(p1.x, p1.y, p2.x, p2.y);
		} else {
			System.out.println("Line Not Accepted.");
		}
	}
	
	private static int computeRegionCode(int x, int y) {
		int code = INSIDE;
		
		if(x < X_MIN)
			code |= LEFT;
		else if(x > X_MAX)
			code |= RIGHT;
		
		if(y < Y_MIN)
			code |= TOP;
		else if(y > Y_MAX)
			code |= BOTTOM;
		
		return code;
	}
}
