import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Point;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

import components.BresenhamLine;
import components.DDALine;
import components.MidPointCircle;
import components.MidPointEllipse;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		BufferedImage buffered_image = new BufferedImage(400, 400, BufferedImage.TYPE_INT_RGB);
		
		ImageIcon image = new ImageIcon(buffered_image);
		
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(buffered_image.getWidth(), buffered_image.getHeight());
		frame.setLayout(new FlowLayout());
		frame.add(new JLabel(image));
		
		frame.setVisible(true);
		
		testDDALine(buffered_image, frame);
		testBresenhamLine(buffered_image, frame);
		testMidPointCircle(buffered_image, frame);
		testMidPointEllipse(buffered_image, frame);
			
		System.out.println("!!!!!!!!DONE!!!!!!!!");
	}
	
	private static void testDDALine(BufferedImage buffered_image, JFrame frame) {
		DDALine.drawDDALine(buffered_image, frame, new Point(10, 10), new Point(200, 300), Color.ORANGE, 2);
		DDALine.drawDDALine(buffered_image, frame, new Point(10, 300), new Point(200, 10), Color.PINK, 2);
		DDALine.drawDDALine(buffered_image, frame, new Point(10, 10), new Point(10, 300), Color.CYAN, 2);
		DDALine.drawDDALine(buffered_image, frame, new Point(10, 300), new Point(200, 300), Color.YELLOW, 2);
		DDALine.drawDDALine(buffered_image, frame, new Point(10, 10), new Point(200, 10), Color.WHITE, 2);
		DDALine.drawDDALine(buffered_image, frame, new Point(200, 10), new Point(200, 300), Color.LIGHT_GRAY, 2);
		
		Color arr[] = new Color[4];
		arr[0] = Color.RED;
		arr[1] = Color.GREEN;
		arr[2] = Color.pink;
		arr[3] = Color.YELLOW;
	
		for(int i = 0; i < 18; i++ ) {
			DDALine.drawDDALine(buffered_image, frame, new Point(200, 300), new Point(20 + (i*10), 10), arr[i%4], 1);
			DDALine.drawDDALine(buffered_image, frame, new Point(190 - (i*10), 10), new Point(10, 300), arr[i%4], 1);
		}
	}
	
	private static void testBresenhamLine(BufferedImage buffered_image, JFrame frame) {
		BresenhamLine.drawBresLine(buffered_image, frame, new Point(10, 10), new Point(200, 300), Color.ORANGE, 2);
		BresenhamLine.drawBresLine(buffered_image, frame, new Point(10, 300), new Point(200, 10), Color.PINK, 2);
		BresenhamLine.drawBresLine(buffered_image, frame, new Point(10, 10), new Point(10, 300), Color.CYAN, 2);
		BresenhamLine.drawBresLine(buffered_image, frame, new Point(10, 300), new Point(200, 300), Color.YELLOW, 2);
		BresenhamLine.drawBresLine(buffered_image, frame, new Point(10, 10), new Point(200, 10), Color.WHITE, 2);
		BresenhamLine.drawBresLine(buffered_image, frame, new Point(200, 10), new Point(200, 300), Color.LIGHT_GRAY, 2);
		
		Color arr[] = new Color[4];
		arr[0] = Color.RED;
		arr[1] = Color.GREEN;
		arr[2] = Color.pink;
		arr[3] = Color.YELLOW;
	
		for(int i = 0; i < 18; i++ ) {
			BresenhamLine.drawBresLine(buffered_image, frame, new Point(200, 300), new Point(20 + (i*10), 10), arr[i%4], 1);
			BresenhamLine.drawBresLine(buffered_image, frame, new Point(190 - (i*10), 10), new Point(10, 300), arr[i%4], 1);
		}
	}
	
	private static void testMidPointCircle(BufferedImage buffered_image, JFrame frame) {
		MidPointCircle.drawMidPointCircle(buffered_image, frame, new Point(200, 200), 100, Color.GREEN, 10);
		MidPointCircle.drawMidPointCircle(buffered_image, frame, new Point(200, 200), 80, Color.PINK, 10);
		MidPointCircle.drawMidPointCircle(buffered_image, frame, new Point(200, 200), 60, Color.BLUE, 10);
		MidPointCircle.drawMidPointCircle(buffered_image, frame, new Point(200, 200), 40, Color.YELLOW, 10);
	}
	
	private static void testMidPointEllipse(BufferedImage buffered_image, JFrame frame) {
		MidPointEllipse.drawMidPointEllipse(buffered_image, frame, new Point(200, 200), 100, 70, Color.PINK, 2);
		MidPointEllipse.drawMidPointEllipse(buffered_image, frame, new Point(200, 200), 70, 100, Color.GREEN, 2);
		MidPointEllipse.drawMidPointEllipse(buffered_image, frame, new Point(200, 200), 100, 100, Color.YELLOW, 2);
	}

}
